import 'package:flutter/material.dart';
import 'package:mavitrika/src/views/screens/splash.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Mavitrika', theme: ThemeData(), home: SplashScreen());
  }
}
