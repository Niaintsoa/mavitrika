import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: const <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Text(
                  'Mavitrika',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                  ),
                ),
              ),
              
              ListTile(
                leading: Icon(Icons.settings),
                title: Text('Firafitra'),
              ),
              ListTile(
                leading: Icon(
                  Icons.help,
                  color: Colors.blue,
                ),
                title: Text(
                  'Mombamomba anay',
                ),
              )
            ],
          ),
        ),
        appBar: AppBar(
          title: Text("Fandraisana"),
          actions: <Widget>[Icon(Icons.supervised_user_circle)],
        ),
        body: Container(
          child: Text("Eto no fandraisana voalohany ooh"),
        ),
      ),
    );
  }
}
