import 'dart:async';

import 'package:mavitrika/src/views/screens/home.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  void initState(){
    super.initState();
    Timer(Duration(seconds: 10),
    ()=> {
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=> HomeScreen()))
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Text('E-Devy',
            style: TextStyle(
              color: Colors.grey,
              fontSize: 29,
              letterSpacing: 10
            )
            ,)
          ],
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
        ),
      ),
    );
  }
}